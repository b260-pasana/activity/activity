<!-- connect the code.php -->
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S01: PHP Basics and Selection Control Structures ACTIVITY</title>
</head>
<body>
    <h1>Full Address</h1>
    <p><?= getFullAddress('Philippines', 'Marilao', 'Bulacan', 'Blk123 L45 Example Street Sample Homes')?></p>

    <h1>Letter-Based Grading</h1>
    <p><?= getLetterGrade(72) ?></p>
</body>
</html>